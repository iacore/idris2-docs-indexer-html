#!/usr/bin/env python3

import sys, os, glob, subprocess

def eprint(*args, **kwargs):
    return print(*args, file=sys.stderr, **kwargs)

def concat_rawindices(html_root_dir, rawindex_dir):
    rawindex_dir = os.path.realpath(rawindex_dir)
    cwd = os.getcwd()
    for dir in glob.glob(f'{html_root_dir}/*/'):
        _prefix, package_name = os.path.split(dir)
        if not package_name:
            _prefix, package_name = os.path.split(_prefix)
        assert package_name
        indexfile = os.path.join(rawindex_dir, f"{package_name}.rawindex")
        eprint(package_name, dir, indexfile)
        # os.chdir()
        subprocess.Popen([
            "idris2-docs-indexer-html", "-a", ".", "-o", indexfile
        ], cwd=dir)
        # os.system(f"")
        

if __name__ == "__main__":
    concat_rawindices(sys.argv[1], "data")
