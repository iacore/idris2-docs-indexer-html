use std::collections::HashSet;
use std::hash::Hash;

use eyre::Result;
use tantivy::collector::TopDocs;
use tantivy::query::QueryParser;
use tantivy::LeasedItem;

use crate::index::{TypedSchema, IndexedDatabase, IdrisDeclWithMetadata};

pub struct SimpleSearchEngine<'a> {
    // pub reader: tantivy::IndexReader,
    pub searcher: LeasedItem<tantivy::Searcher>,
    pub query_parser: QueryParser,
    pub schema: &'a TypedSchema,
}

impl<'a> SimpleSearchEngine<'a> {
    pub fn new(db: &'a IndexedDatabase) -> Result<Self> {
        let reader = db
            .index
            .reader_builder()
            .reload_policy(tantivy::ReloadPolicy::OnCommit)
            .try_into()?;
        let searcher = reader.searcher();
        let query_parser = db.make_query_parser();
        Ok(Self {
            // reader,
            searcher,
            query_parser,
            schema: &db.typed_schema,
        })
    }

    pub fn query_top_n(&self, query_string: &str, at_most: usize) -> Result<Vec<IdrisDeclWithMetadata>, eyre::Error> {
        let query = self.query_parser.parse_query(&query_string)?;
        let top_docs = self.searcher.search(&query, &TopDocs::with_limit(at_most))?;
        
        let mut result = vec![];
        let mut deduper = Deduper::new();
        
        for (score, doc_address) in top_docs {
            let doc = self.searcher.doc(doc_address)?;
            let idris_decl = self.schema.doc_to_decl(doc, score)?;
            if !deduper.is_duplicate((idris_decl.package.clone(), idris_decl.name.clone())) {
                result.push(idris_decl);
            }
        }
        Ok(result)
    }
}

pub struct Deduper<T: Hash + Eq> {
    seen: HashSet<T>
}

impl<T: Hash + Eq> Deduper<T> {
    pub fn new() -> Self {
        Self{
            seen: Default::default()
        }
    }

    /// add the hash of value to `seen`
    /// # return
    /// if it has seen it before
    pub fn is_duplicate(&mut self, value: T) -> bool {
        !self.seen.insert(value)
    }
}
