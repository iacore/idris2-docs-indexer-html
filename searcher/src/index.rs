use std::{collections::HashMap, path::PathBuf};

use idris2_docs_indexer_html::parse::IdrisDecl;

#[cfg(feature="search")]
use tantivy::{
    query::QueryParser,
    schema::{Field, Schema, STORED, TEXT},
    Document, Index,
};
use tendril::StrTendril;
use eyre::{eyre, Result};

#[derive(Clone)]
pub struct TypedSchema {
    pub schema: Schema,
    pub m_filename: Field,
    pub m_html_id: Field,
    pub m_package: Field,
    pub m_namespace: Field,
    pub m_prefix: Field,
    pub m_name: Field,
    pub m_type: Field,
}

impl TypedSchema {
    pub fn new() -> Self {
        let mut schema_builder = Schema::builder();
        let m_filename = schema_builder.add_text_field("filename", STORED);
        let m_html_id = schema_builder.add_text_field("html_id", STORED);
        let m_package = schema_builder.add_text_field("package", TEXT | STORED);
        let m_namespace = schema_builder.add_text_field("namespace", TEXT | STORED);
        let m_prefix = schema_builder.add_text_field("prefix", TEXT | STORED);
        let m_name = schema_builder.add_text_field("name", TEXT | STORED);
        let m_type = schema_builder.add_text_field("type", TEXT | STORED);
        let schema = schema_builder.build();
        Self {
            schema,
            m_filename,
            m_html_id,
            m_package,
            m_namespace,
            m_prefix,
            m_name,
            m_type,
        }
    }

    pub fn decl_to_doc(
        &self,
        namespace_filename: impl ToString,
        namespace: impl ToString,
        package: impl ToString,
        idris_decl: IdrisDecl,
    ) -> Document {
        let IdrisDecl {
            name,
            typ,
            prefix,
            html_id,
        } = idris_decl;
        let mut tt_doc = Document::default();
        tt_doc.add_text(self.m_filename, namespace_filename);
        tt_doc.add_text(self.m_html_id, html_id);
        tt_doc.add_text(self.m_namespace, namespace);
        tt_doc.add_text(self.m_package, package);
        tt_doc.add_text(self.m_prefix, prefix);
        tt_doc.add_text(self.m_name, name);
        tt_doc.add_text(self.m_type, typ);
        tt_doc
    }

    pub fn doc_to_decl(&self, doc: Document, match_score: f32) -> Result<IdrisDeclWithMetadata> {
        macro_rules! get_text_field {
            ($field:expr) => {
                {
                    let Some(field_s) = doc.get_first($field).and_then(|value| value.as_text()) else {
                        return Err(eyre!("Document malformed: {:?}", doc));
                    };
                    field_s.to_owned()
                }
            }
        }
        let namespace = get_text_field!(self.m_namespace);
        let prefix = get_text_field!(self.m_prefix);
        let name = get_text_field!(self.m_name);
        let typ = get_text_field!(self.m_type);
        let html_id = get_text_field!(self.m_html_id);
        let html_filename = get_text_field!(self.m_filename);
        let package = get_text_field!(self.m_package);

        Ok(IdrisDeclWithMetadata{
            match_score,
            html_filename,
            package,
            namespace,
            name,
            typ,
            prefix,
            html_id,
        })
    }

    // fn all_fields(&self) -> Vec<Field> {
    //     vec![
    //         self.m_filename,
    //         self.m_html_id,
    //         self.m_package,
    //         self.m_namespace,
    //         self.m_prefix,
    //         self.m_name,
    //         self.m_type,
    //     ]
    // }

    fn default_fields_to_use(&self) -> Vec<Field> {
        vec![
            // self.m_package,
            self.m_namespace,
            // self.m_prefix,
            self.m_name,
            self.m_type,
        ]
    }
}

pub struct IdrisDeclWithMetadata {
    pub match_score: f32,
    pub html_filename: String,
    pub package: String,
    pub namespace: String,
    pub name: String,
    pub typ: String,
    pub prefix: String,
    pub html_id: String,
}

pub struct IndexedDatabase {
    pub index: Index,
    pub typed_schema: TypedSchema,
    pub package_to_filename: HashMap<StrTendril, PathBuf>,
}
impl IndexedDatabase {
    pub fn make_query_parser(&self) -> QueryParser {
        QueryParser::for_index(&self.index, self.typed_schema.default_fields_to_use())
    }
}
