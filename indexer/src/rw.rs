use eyre::Result;
use serde::Serialize;

use crate::parse::IdrisHTMLDocumentWithMetadata;

use std::io::Write;

pub fn write_document(
    writer: &mut dyn Write,
    doc: IdrisHTMLDocumentWithMetadata,
) -> Result<(), eyre::Error> {
    let mut serializer = serde_json::Serializer::new(writer);
    doc.serialize(&mut serializer)?;
    let writer = serializer.into_inner();
    writer.write_all(b"\n")?;
    Ok(())
}
