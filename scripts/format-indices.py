#!/usr/bin/env python3

import sys, os, glob, re, subprocess

def replace_and_print(fin, fout, package_name):
    s = fin.read()
    s = s.replace('"filename":"./', '"filename":"' + package_name + "/")
    fout.write(s)

def eprint(*args, **kwargs):
    return print(*args, file=sys.stderr, **kwargs)

def concat_rawindices(rawindex_dir, out_dir):
    for indexfile in glob.glob(f'{rawindex_dir}/*.rawindex'):
        try:
            package_name = re.search(r"/([a-z-]+)\.rawindex", indexfile)[1]
        except TypeError:
            eprint(f"Package name not quite right: {indexfile}")
            continue
        assert package_name
        eprint(indexfile, package_name)
        command = ["idris2-docs-searcher", "/dev/stdin", "-o", os.path.join(out_dir, f"{package_name}.json")]
        eprint("Running", repr(command))
        r_fd, w_fd = os.pipe()
        with os.fdopen(w_fd, "w") as pipef, os.fdopen(r_fd, "r") as pipe_r:
            try:
                with open(indexfile) as f:
                    subprocess.Popen(command, stdin=pipe_r)
                    replace_and_print(f, pipef, package_name)
            except FileNotFoundError:
                eprint(f"index file missing: {indexfile}")

if __name__ == "__main__":
    concat_rawindices("data", "processed")
