use std::path::PathBuf;

use serde::{Serialize, Deserialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct IdrisHTMLDocumentWithMetadata {
    pub filename: PathBuf,
    pub subtype: IdrisHTMLDocument,
}

#[derive(Debug, Serialize, Deserialize)]
pub enum IdrisHTMLDocument {
    PackageIndex(IdrisPackageIndex),
    Namespace(IdrisNamespace),
}

#[derive(Debug, Serialize, Deserialize)]
pub struct IdrisPackageIndex {
    pub name: String,
    pub namespaces: Vec<NamespaceReference>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct NamespaceReference {
    pub name: String,
    /// untouched from HTML <a href="href"></a>
    /// relative URL
    pub html_href: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct IdrisNamespace {
    pub name: String,
    pub decls: Vec<IdrisDecl>,
}

/// 1 declaration. Function or Type or Term.
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct IdrisDecl {
    pub name: String,
    pub typ: String,
    /// prefix to name like "interface", "0", "1"
    pub prefix: String,
    /// for use with url#hash
    pub html_id: String,
}

pub fn read_document_stream<R: std::io::Read>(
    file: R,
) -> serde_json::StreamDeserializer<'static, serde_json::de::IoRead<R>, IdrisHTMLDocumentWithMetadata>
{
    let stream =
        serde_json::Deserializer::from_reader(file).into_iter::<IdrisHTMLDocumentWithMetadata>();
    stream
}
