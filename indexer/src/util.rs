use std::collections::HashSet;
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};
use std::marker::PhantomData;

#[derive(Debug)]
pub struct Deduper<T: Hash> {
    seen: HashSet<u64>,
    phantom: PhantomData<T>,
}

impl<T: Hash> Deduper<T> {
    pub fn new() -> Self {
        Self{
            seen: Default::default(),
            phantom: PhantomData::default(),
        }
    }

    /// add the hash of value to `seen`
    /// # return
    /// if it has seen it before
    pub fn is_duplicate(&mut self, value: T) -> bool {
        let mut hasher = DefaultHasher::default();
        value.hash(&mut hasher);
        !self.seen.insert(hasher.finish())
    }
}
