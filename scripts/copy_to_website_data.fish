#!/usr/bin/fish

set p /home/user/computing/web/idris2-quickdocs # quick-docs repo
rsync -a processed/ $p/data/
rsync -a idris2-pack-docs-main/docs/ $p/data/
pushd $p
poetry run ./mkhome.py data
popd