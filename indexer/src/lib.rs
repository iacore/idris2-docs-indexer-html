#![warn(clippy::unwrap_used)]
#![warn(clippy::expect_used)]
#![warn(clippy::nursery)]
#![warn(clippy::pedantic)]
#![allow(clippy::enum_glob_use)]
#![feature(let_else)]

pub mod parse;
pub mod rw;
pub mod util;

#[cfg(test)]
mod tests {
    // use super::*;

    // #[test]
    // fn it_works() {
    //     let result = add(2, 2);
    //     assert_eq!(result, 4);
    // }
}
