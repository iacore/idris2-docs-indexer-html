#!/usr/bin/fish

set idris_dir $argv[1]
if test ! $idris_dir
    echo Index libs that come with Idris2
    echo Usage: (status -f) IDRIS2_REPO_DIR
    exit 1
end
set idris_dir (string trim -r -c '/' $idris_dir)

if command -q idris2-docs-indexer-html
else
    echo Cannot find \'idris2-docs-indexer-html\' to use
    exit 1
end

for lib in base contrib linear network prelude test
    ./index-lib.fish $idris_dir/libs/$lib
end
