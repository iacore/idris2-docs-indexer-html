#!/usr/bin/fish

set idris_dir $argv[1]
if test ! $idris_dir
    echo Index 1 lib
    echo Usage: (status -f) IDRIS2_REPO_DIR
    exit 1
end

if command -q idris2-docs-indexer-html
else
    echo Cannot find \'idris2-docs-indexer-html\' to use
    exit 1
end

set data_dir (realpath (dirname (status -f))/data)

function index -a lib
    echo Processing: $lib
    if test -e build/docs
        rsync -a build/docs/ $data_dir/$lib/
    else
        idris2 --mkdoc $lib.ipkg
    end
    and idris2-docs-indexer-html -a build/docs -o $data_dir/$lib.rawindex
end

pushd $idris_dir || exit 1
index (basename $idris_dir)
