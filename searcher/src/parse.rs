#[cfg(feature="search")]
use crate::index::{IndexedDatabase, TypedSchema};
// use idris2_docs_indexer_html::parse;
use idris2_docs_typedef::IdrisDecl;
use idris2_docs_typedef::IdrisHTMLDocument;
use idris2_docs_typedef::IdrisHTMLDocumentWithMetadata;

use std::collections::HashMap;
use std::path::PathBuf;
#[cfg(feature="search")]
use std::path::Path;

#[cfg(feature="search")]
use eyre::{Result, WrapErr};
use serde::Serialize;
#[cfg(feature="search")]
use tantivy::Index;
use tendril::SliceExt;
use tendril::StrTendril;
use tracing::warn;

#[derive(Default)]
pub struct IdrisSearchIndex {
    pub package_to_filename: HashMap<StrTendril, PathBuf>,
    pub namespace_to_filename: HashMap<StrTendril, PathBuf>,
    pub namespace_to_package: HashMap<StrTendril, StrTendril>,
    pub namespace_to_decls: HashMap<StrTendril, Vec<IdrisDecl>>,
}

impl IdrisSearchIndex {
    pub fn new() -> Self {
        Default::default()
    }

    /// Add document to the index
    pub fn index_doc(&mut self, doc: IdrisHTMLDocumentWithMetadata) {
        match doc.subtype {
            IdrisHTMLDocument::PackageIndex(package) => {
                let package_name = package.name.to_tendril();
                if let Some(other_filename) = self
                    .package_to_filename
                    .insert(package_name.clone(), doc.filename.clone())
                {
                    warn!(
                        "Package {} filename conflict.\nIs: {}.\nWas: {}.\nSearch result of this thing may be wrong.",
                        package_name,
                        doc.filename.to_string_lossy(),
                        other_filename.to_string_lossy()
                    );
                }
                for namespace in &package.namespaces {
                    let namespace_name = namespace.name.to_tendril();
                    if let Some(other_package_name) = self
                        .namespace_to_package
                        .insert(namespace_name.clone(), package_name.clone())
                    {
                        warn!(
                            "Namespace {} package conflict.\nIs: {}.\nWas: {}.\nSearch result of this thing may be wrong.",
                            namespace_name,
                            package_name,
                            other_package_name
                        );
                    }
                }
            }
            IdrisHTMLDocument::Namespace(namespace) => {
                let namespace_name = namespace.name.to_tendril();
                if let Some(other_filename) = self
                    .namespace_to_filename
                    .insert(namespace_name.clone(), doc.filename.clone())
                {
                    warn!(
                        "Namespace {} filename conflict.\nIs: {}.\nWas: {}.\nSearch result of this thing may be wrong.",
                        namespace_name,
                        doc.filename.to_string_lossy(),
                        other_filename.to_string_lossy()
                    );
                }
                for decl in namespace.decls {
                    self.index_decl(namespace_name.clone(), decl);
                }
            }
        }
    }

    pub fn index_decl(&mut self, in_namespace: impl Into<StrTendril>, decl: IdrisDecl) {
        let in_namespace = in_namespace.into();
        if let Some(decls) = self.namespace_to_decls.get_mut(&in_namespace) {
            decls.push(decl);
        } else {
            self.namespace_to_decls.insert(in_namespace, vec![decl]);
        }
    }

    // todo: too inefficient
    pub fn decls(&self) -> Vec<IdrisDeclWithMetadata> {
        let mut r = vec![];

        for (namespace, idris_decls) in &self.namespace_to_decls {
            let filename = self.namespace_to_filename.get(namespace).map_or_else(
                || {
                    warn!("Namespace {} has no associated filename", namespace);
                    "".into()
                },
                |s| s.to_string_lossy(),
            );

            let package = self.namespace_to_package.get(namespace).map_or_else(
                || {
                    warn!("Namespace {} has no associated package", namespace);
                    ""
                },
                |s| s.as_ref(),
            );
            for idris_decl in idris_decls {
                let item = IdrisDeclWithMetadata {
                    html_filename: filename.to_string(),
                    package: package.to_string(),
                    namespace: namespace.to_string(),
                    inner: idris_decl.clone(),
                };
                r.push(item);
            }
        }
        r
    }

    #[cfg(feature="search")]
    pub fn into_tantivy(self, path_index_cache: impl AsRef<Path>) -> Result<IndexedDatabase> {
        let typed_schema = TypedSchema::new();

        let index = match Index::create_in_dir(&path_index_cache, typed_schema.schema.clone()) {
            Ok(index) => {
                let mut index_writer = index.writer(10_000_000)?;

                for decl in self.decls() {
                    let tt_doc = typed_schema.decl_to_doc(
                        decl.html_filename,
                        decl.namespace,
                        decl.package,
                        decl.inner,
                    );
                    index_writer.add_document(tt_doc)?;
                }
                index_writer.commit()?;
                index
            }
            Err(err) => Index::open_in_dir(&path_index_cache)
                .with_context(|| format!("DB Exist: {:?}", err))?,
        };

        Ok(IndexedDatabase {
            index,
            typed_schema,
            package_to_filename: self.package_to_filename,
        })
    }
}

#[derive(Serialize)]
pub struct IdrisDeclWithMetadata {
    pub html_filename: String,
    pub package: String,
    pub namespace: String,
    #[serde(flatten)]
    pub inner: IdrisDecl,
}
