#![warn(clippy::unwrap_used)]
#![warn(clippy::expect_used)]
#![warn(clippy::nursery)]
#![warn(clippy::pedantic)]
#![feature(let_else)]

use std::collections::BTreeSet;
use std::fs::File;
use std::io::{stdout, Write};
use std::path::{Path, PathBuf};
use std::sync::{Arc, Mutex};

use clap::Parser;
use eyre::{eyre, Result};
use idris2_docs_indexer_html::parse::{process_file, read_document_stream, IdrisHTMLDocument};
use idris2_docs_indexer_html::rw::write_document;
use idris2_docs_indexer_html::util::Deduper;
use rayon::prelude::*;
use tracing::metadata::LevelFilter;
use tracing::{error, info, info_span, trace, warn};

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Search for **/*.html in this directory
    #[clap(short = 'a', long)]
    add_root: Vec<String>,

    /// Add individual files to search. You can use shell syntax like {a,b}/*.html to your advantage
    #[clap(value_parser)]
    file: Vec<PathBuf>,

    /// Show complete error trace
    #[clap(short, value_parser)]
    verbose: bool,

    /// Which file to output to?
    #[clap(short, value_parser)]
    output: Option<PathBuf>,

    /// Parse an index file and print debug outputs
    #[clap(long, value_parser)]
    test_parse: Option<PathBuf>,
}

fn init(args: &Args) -> Result<()> {
    use tracing_subscriber::{prelude::*, Registry};
    use tracing_tree::HierarchicalLayer;

    let subscriber =
        Registry::default().with(HierarchicalLayer::new(2).with_filter(if args.verbose {
            LevelFilter::TRACE
        } else {
            LevelFilter::INFO
        }));
    tracing::subscriber::set_global_default(subscriber)?;
    Ok(())
}

type SendableWrite = Box<dyn Write + Send + Sync>;
type DeclDeduper = Deduper<(String, String)>;

fn main() -> Result<()> {
    let args: Args = Args::parse();
    init(&args)?;
    if let Some(path) = args.test_parse.clone() {
        return main_test_parse(args, &path);
    }

    // Individual files to search
    let mut paths: BTreeSet<PathBuf> = BTreeSet::default();

    // TODO: par iter this?
    for dir_root in &args.add_root {
        add_glob(&mut paths, dir_root)?;
    }
    for extra_file in &args.file {
        paths.insert(extra_file.clone());
    }

    if paths.is_empty() && args.add_root.is_empty() {
        warn!("No files given. I will do nothing. Specify -h to see usage.");
        return Ok(());
    }

    let writer: SendableWrite = if let Some(output_path) = &args.output {
        let file = File::create(output_path)?;
        Box::new(file)
    } else {
        Box::new(stdout())
    };
    let deduper: DeclDeduper = idris2_docs_indexer_html::util::Deduper::new();
    let arc_writer = Arc::new(Mutex::new((writer, deduper)));

    paths.par_iter().try_for_each(|path| {
        read_document_then_write(&args, path, arc_writer.clone())
    })?;
    Ok(())
}

fn read_document_then_write(
    args: &Args,
    path: &Path,
    arc_writer: Arc<Mutex<(SendableWrite, DeclDeduper)>>,
) -> Result<()> {
    let _span = info_span!("Processing", ?path).entered();

    // Read document
    let mut doc = match process_file(&path) {
        Ok(it) => it,
        Err(err) => {
            if args.verbose {
                error!("{:?}", err);
            } else {
                error!("{:#}", err);
            }
            return Ok(());
        }
    };

    // Write JSON
    let mut writer_lock = arc_writer.lock().map_err(|e| eyre!("{:?}", e))?;
    let (writer, dedupper) = &mut (*writer_lock);

    match &mut doc.subtype {
        IdrisHTMLDocument::PackageIndex(_) => {}
        IdrisHTMLDocument::Namespace(namespace) => {
            namespace
                .decls
                .retain(|decl| !dedupper.is_duplicate((namespace.name.clone(), decl.name.clone())));
        }
    }
    write_document(writer, doc)?;

    Ok(())
}

fn add_glob(paths: &mut BTreeSet<PathBuf>, dir_root: &str) -> Result<(), eyre::Error> {
    let glob_html = globwalk::GlobWalkerBuilder::new(dir_root, "**/*.html").build()?;
    // TODO: par iter this?
    for filename in glob_html {
        let filename = filename?;
        let file_type = filename.file_type();
        let path = filename.path();
        // skip source files
        if path.to_string_lossy().ends_with(".src.html") {
            continue;
        }
        if file_type.is_file() {
            paths.insert(path.to_owned());
        } else {
            warn!("File type {:?} is not file", file_type);
        }
    }
    Ok(())
}

fn main_test_parse(_args: Args, path: &Path) -> Result<(), eyre::Error> {
    let file = File::open(path)?;
    let stream = read_document_stream(file);

    let mut total = 0;
    for doc in stream {
        let doc = doc?;
        trace!("Parsed {:?}", doc);
        total += 1;
    }
    info!("Parsed {} entries", total);
    Ok(())
}
