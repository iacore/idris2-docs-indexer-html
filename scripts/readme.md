
## Usage

```
# get html docs from stefan-hoeck/idris2-pack-docs
wget2 https://github.com/stefan-hoeck/idris2-pack-docs/archive/refs/heads/main.zip
unzip main.zip

# build html docs
idris2 --mkdoc xxx.ipkg

# index idris libs
./index-idris-libs.fish ~/path/to/Idris2/

# index 3rd party lib (legacy)
./index-lib.fish ~/path/to/any/library/like/sop/

# index 3rd party lib
./index-libs.py idris2-pack-docs-main/docs/

# aggregate index
python concat.py > aggregated.rawindex

# format for use with quick-docs
idris2-docs-searcher aggregated.rawindex -o index2.json
```

## How I generate index files

In this directory:

```
# get docs
wget2 https://github.com/stefan-hoeck/idris2-pack-docs/archive/refs/heads/main.zip
unzip main.zip

# install tools
cargo install --path ../indexer
cargo install --path ../searcher

mkdir processed # final output
mkdir data # raw index files (for the Rust tool)
./index-libs.py idris2-pack-docs-main/docs/
./format-indices.py
```
