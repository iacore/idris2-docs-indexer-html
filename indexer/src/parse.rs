use std::io::Read;
use std::path::Path;
use std::fs::File;

use eyre::{eyre, Context, Result, ContextCompat};
use nipper::{Document, Node, Selection};
use tendril::{fmt::UTF8, StrTendril, Tendril};
use tracing::{warn, info};

pub use idris2_docs_typedef::*;

// todo: trace and instrument everything

/// Attemp to parse 1 HTML file
/// 
/// # Errors
/// 
/// When the file failed to open, fail to parse, or some irrecovable error occured, this will return error.
/// This error is not fatal
pub fn process_file(path: &Path) -> Result<IdrisHTMLDocumentWithMetadata> {
    let mut buf: Vec<u8> = vec![];

    let mut file = File::open(path).context("Open file failed")?;
    
    let _ = file
        .read_to_end(&mut buf)
        .context("Read file failed")?;
    let tendril: Tendril<UTF8> =
        Tendril::try_from_byte_slice(&buf).map_err(|_| eyre!("Content not UTF8"))?;
    let document = Document::from(tendril);
    let subtyped_doc = parse_document(&document)?;

    let doc = IdrisHTMLDocumentWithMetadata {
        filename: path.to_owned(),
        subtype: subtyped_doc,
    };
    Ok(doc)
}

fn parse_document(document: &Document) -> Result<IdrisHTMLDocument, eyre::Error> {
    use IdrisHTMLDocument::*;
    let el_body = document.select("body");
    if el_body.length() != 1 { return Err(eyre!("Expect 1 body tag. Actual: {}.", el_body.length())) }
    if let Some(class) = el_body.attr("class") {
        let subtyped_doc = match class.to_string().as_str() {
            "index" => PackageIndex(parse_doc_type_index(document)?),
            "namespace" => Namespace(parse_doc_type_namespace(document)?),
            class => {
                return Err(eyre!("Weird .body[class] {}", class));
            }
        };
        Ok(subtyped_doc)
    } else {
        let attrs = el_body.get(0).unwrap().attrs();
        info!(?attrs);
        Err(eyre!("Cannot detect HTML file type"))
    }
}

/// parse entire HTML (package index)
#[allow(clippy::unnecessary_wraps)]
fn parse_doc_type_index(document: &Document) -> Result<IdrisPackageIndex> {
    let package_name = document.select("title").text();
    let package_name = package_name.trim();

    let namespaces = document
        .select(".code")
        .iter()
        .filter_map(|el_a| {
            let html_href = el_a.attr("href").or_else(|| {
                warn!(?el_a, "a.code doesn't have href");
                None
            })?;
            Some(NamespaceReference {
                name: el_a.text().into(),
                html_href: html_href.into(),
            })
        })
        .collect();
    // let namespaces:

    Ok(IdrisPackageIndex {
        name: package_name.to_owned(),
        namespaces,
    })
}

/// parse entire HTML (list of functions and types)
fn parse_doc_type_namespace(document: &Document) -> Result<IdrisNamespace> {
    let namespace_name = maybe_first_node(&document.select("h1"))
        .context("No namespace")?
        .text();

    let decls = document.select("dt").iter();

    // TODO: use par_iter
    // Selection is not Send though
    let decls = decls.filter_map(|el_dt| parse_doc_decl(&el_dt));
    Ok(IdrisNamespace {
        name: namespace_name.trim().into(),
        decls: decls.collect(),
    })
}

// TODO: refactor
// warn! -> .with_context
// Option -> Result
/// parse one declaration in HTML
fn parse_doc_decl(el_dt: &Selection) -> Option<IdrisDecl> {
    let Some(id) = el_dt.attr("id") else { warn!(?el_dt, "Elem <dt> has no id"); return None};
    let html_id = id.to_string();
    if html_id.starts_with("$resolved") {
        return None;
    }
    let mut prefix = StrTendril::new();
    let mut postfix = StrTendril::new();
    let mut got_name = false;
    let mut name = None;
    let sel_code = el_dt.select("code");
    let Some(node_code) = maybe_first_node(&sel_code) else {
        warn!(?el_dt, "Cannot find element: code");
        return None;
    };
    for decl_fragment in node_code.children() {
        let decl_fragment_text = decl_fragment.text();
        // let maybe_name = decl_node.select("span.name");
        let is_name_of_decl = if decl_fragment.is_element() {
            let el_decl_fragment = Selection::from(decl_fragment);
            el_decl_fragment.select("span.name").exists()
        } else {
            false
        };
        if is_name_of_decl && !got_name {
            got_name = true;
            name = Some(decl_fragment_text);
        } else {
            (if got_name { &mut postfix } else { &mut prefix }).push_tendril(&decl_fragment_text);
        }
    }

    let Some(name) = name else { warn!(?el_dt, "No direct child of <dt> has name"); return None };

    let prefix = sanitize_idris_span(&prefix);
    let postfix = sanitize_idris_span(&postfix);
    let name = sanitize_idris_span(&name); // maybe I can sanitize name better

    let Some(postfix) = postfix.strip_prefix(": ") else {
        warn!(?prefix, ?name, ?postfix, "Cannot recognize type of decl");
        return None;
    };

    Some(IdrisDecl {
        name: name.into(),
        typ: postfix.to_owned(),
        prefix: prefix.into(),
        html_id,
    })
}

fn sanitize_idris_span(s: &StrTendril) -> StrTendril {
    s.trim()
        .chars()
        .map(|c| match c {
            '\u{2002}' => ' ',
            other => other,
        })
        .collect()
}

fn maybe_first_node<'a>(selection: &'a Selection) -> Option<Node<'a>> {
    if selection.length() > 0 {
        Some(selection.nodes()[0].clone())
    } else {
        None
    }
}
