#!/usr/bin/env python3

import sys, os, glob, re

def replace_and_print(fin, fout, package_name):
    s = fin.read()
    s = s.replace('"filename":"./', '"filename":"' + package_name + "/")
    fout.write(s)

def eprint(*args, **kwargs):
    return print(*args, file=sys.stderr, **kwargs)

def concat_rawindices(rawindex_dir):
    for indexfile in glob.glob(f'{rawindex_dir}/*.rawindex'):
        try:
            package_name = re.search(r"/([a-z-]+)\.rawindex", indexfile)[1]
        except TypeError:
            eprint(f"Package name not quite right: {indexfile}")
            continue
        eprint(indexfile, package_name)
        assert package_name
        try:
            with open(indexfile) as f:
                replace_and_print(f, sys.stdout, package_name)
        except FileNotFoundError:
            eprint(f"index file missing: {indexfile}")

if __name__ == "__main__":
    concat_rawindices("data")
