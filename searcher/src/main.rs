#![warn(clippy::unwrap_used)]
#![warn(clippy::expect_used)]
#![warn(clippy::nursery)]
#![warn(clippy::pedantic)]
#![feature(let_else)]

pub mod parse;
#[cfg(feature="search")]
pub mod index;
#[cfg(feature="search")]
pub mod query;

use std::fs::File;
use std::path::PathBuf;

use clap::Parser;
use eyre::Result;
#[cfg(feature="search")]
use query::SimpleSearchEngine;
use tracing::metadata::LevelFilter;
use tracing::{error, trace};
use serde::Serialize;

use idris2_docs_typedef::{read_document_stream, IdrisHTMLDocumentWithMetadata};

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Index file to use.
    /// If you need to use multiple files, concat them with `cat` first.
    #[clap(value_parser)]
    index_file: PathBuf,

    /// Show complete error trace
    #[clap(short, value_parser)]
    verbose: bool,

    #[clap(short = 'd', long = "db-dir", value_parser, default_value = ".db")]
    path_index_cache: PathBuf,

    /// output indexed file as json for tinysearch
    #[clap(short, value_parser)]
    output: Option<PathBuf>,
}

fn init(args: &Args) -> Result<()> {
    use tracing_subscriber::{prelude::*, Registry};
    use tracing_tree::HierarchicalLayer;

    let subscriber =
        Registry::default().with(HierarchicalLayer::new(2).with_filter(if args.verbose {
            LevelFilter::TRACE
        } else {
            LevelFilter::INFO
        }));
    tracing::subscriber::set_global_default(subscriber)?;

    std::fs::create_dir_all(&args.path_index_cache)?;
    Ok(())
}

fn main() -> Result<()> {
    let args: Args = Args::parse();
    init(&args)?;

    let mut searchindex = parse::IdrisSearchIndex::new();
    let path = &args.index_file;
    let file = File::open(path)?;
    let stream = read_document_stream(file);

    for doc in stream {
        let doc: IdrisHTMLDocumentWithMetadata = doc?;
        trace!("Parsed {:?}", doc);
        searchindex.index_doc(doc);
    }

    if let Some(path_output) = &args.output {
        let file = File::create(path_output)?;
        let mut serializer = serde_json::Serializer::new(file);
        searchindex.decls().serialize(&mut serializer)?;
    } else {
        #[cfg(feature="search")]
        {
            let db = searchindex.into_tantivy(&args.path_index_cache)?;
            let query_handler = SimpleSearchEngine::new(&db)?;
        
            readline_loop(&args, &query_handler)?;
        }


        #[cfg(not(feature="search"))]
        {
            error!("No output specified. No REPL included ('search' feature is disabled when built).")
        }
    }
    Ok(())
}

#[cfg(feature="search")]
use rustyline::{Editor, error::ReadlineError};

#[cfg(feature="search")]
fn readline_loop(args: &Args, se: &SimpleSearchEngine) -> Result<()> {
    let history_file = args.path_index_cache.join("history.txt");

    let mut rl = Editor::<()>::new()?;
    if rl.load_history(&history_file).is_err() {
        // println!("No previous history.");
    }

    'repl_loop: loop {
        let readline = rl.readline("term? ");
        match readline {
            Ok(line) => {
                rl.add_history_entry(&line);
                match se.query_top_n(&line, 100) {
                    Ok(docs) => {
                        use tabular::{Row, Table};
                        let mut table = Table::new("{:>} {:>} : {:<} | {:<}");
                        for doc in docs {
                            table.add_row(
                                Row::new()
                                    .with_cell(doc.prefix)
                                    .with_cell(doc.name)
                                    .with_cell(doc.typ)
                                    .with_cell(doc.namespace),
                            );
                        }
                        print!("{}", table);
                    }
                    Err(err) => {
                        if args.verbose {
                            error!("{:?}", err);
                        } else {
                            error!("{:#}", err);
                        }
                    }
                }
            }
            Err(ReadlineError::Interrupted) => {
                println!("^C");
                break 'repl_loop;
            }
            Err(ReadlineError::Eof) => {
                break 'repl_loop;
            }
            Err(err) => {
                println!("Error: {:?}", err);
                break 'repl_loop;
            }
        }
    }
    rl.save_history(&history_file)?;
    Ok(())
}
