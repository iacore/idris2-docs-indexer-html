# idris2-docs-indexer-html
Indexer for HTML files generated by `idris2 --mkdoc package.ipkg`. Emit many lines of JSON.

## Usage

To index a package, first generate its HTML docs.
```
idris2 --mkdoc package.ipkg
```

Then, index the HTML files by pointing this tool to the docs directory.
```
cargo run -- -a path/to/package/build/docs > out.txt
```

Now `out.txt` should contain a json object on every line.

You can merge several index files using `cat`.

## Serde usage

If you know serde, you can use the types marked with `#[derive(Debug, Serialize, Deserialize)]` to read the data back in. Every line of this tool's output is a `IdrisHTMLDocumentWithMetadata` as JSON.
